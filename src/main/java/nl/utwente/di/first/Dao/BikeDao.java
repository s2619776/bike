package nl.utwente.di.first.Dao;


import nl.utwente.di.first.Model.Bike;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
public enum BikeDao {

    instance;

    private Map<String, Bike> bikes = new HashMap<String, Bike>();

    private BikeDao() {

        Bike bike = new Bike("1", "Learn REST", "Black", "Male");
        bikes.put("1", bike);
        Bike bike2 = new Bike("2", "Do something", "Yellow", "Female");
        bikes.put("2", bike2);

    }

    public Map<String, Bike> getBikes() {
        return bikes;
    }

}
