package nl.utwente.di.first.Resources;


import nl.utwente.di.first.Model.Bike;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class BikeResources {


    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void createBike(@FormParam("id") String id,
                           @FormParam("ownerName") String ownerName,
                           @FormParam("colour") String colour,
                           @FormParam("gender") String gender,
                           @Context HttpServletResponse servletResponse) throws IOException {
        Bike bike = new Bike(id, ownerName, colour, gender);

        Bike.instance.getBikes().put(id, bike );

        servletResponse.sendRedirect("../create_bike.html");

    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Bike> getBikes() {
        List<Bike> bikes = new ArrayList<Bike>();
        bikes.addAll(Bike.instance.getBikes().values());
        return bikes;
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Bike getBikesDetails(String id) {

        Bike result = Bike.instance.getBikes().get(id);

        return result;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public Bike updateBike(Bike bike) {
        Bike b = bike;
        Bike.instance.getBikes().put(b[Object.keys(b)[0]]);
        

        return b
                ;
    }

    @DELETE
    public void deleteBike(String id) {
        Bike b = Bike.instance.getBikes().remove(id);
        if(b==null)
            throw new RuntimeException("Delete: Todo with " + id +  " not found");
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public boolean orderBike(@FormParam("id") String id,
                           @FormParam("ownerName") String ownerName,
                           @FormParam("colour") String colour,
                           @FormParam("gender") String gender,
                           @Context HttpServletResponse servletResponse) throws IOException {
        Bike bike = new Bike(id, ownerName, colour, gender);
        Boolean status = false;
        if (Bike.instance.getBikes().containsKey(id )){
            bike.instance.getBikes().remove(id);
            status = true;
        }

        return status;
    }

}

